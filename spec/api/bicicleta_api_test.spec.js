var Bicicleta = require('../../models/bicicleta');
const { default: Axios } = require('axios');
//var {Bicicleta} = require('../../controllers/api/bicicletaControllerAPI');
var server = require('../../bin/www');

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'negro', 'urbana', [-34.6012424, -58.3861497]);
            Bicicleta.add(a);

            Axios.get('http://localhost:3000/api/bicicletas')
                .then(function (response) {
                    expect(response.status).toBe(200);
                    done();
                })
                .catch(function (error) {
                    console.log(error);
                })
        })
    });
});

describe('POST BICICLETAS /create', () => {
    it('STATUS 200', (done) => {
        //var headers = { 'Content-Type': 'application/json' };
        //var aBici = {id: 10, color: rojo, modelo: urbano, lat: -34, lng: -54};

        Axios.post('http://localhost:3000/api/bicicletas/create', {
            id: 14, color: 'fuego', modelo: 'urbano', lat: -34, lng: -54
        })
            .then(function (response) {
                expect(response.status).toBe(200);
            }).catch(function (error) {
                console.log(error);
            })
        Axios.get('http://localhost:3000/api/bicicletas')
            .then(function (response) {
                expect(response.data.bicicletas.find(bici => bici.id == 14).color).toBe("fuego");
                done();
            })
            .catch(function (error) {
                console.log(error);
            })
    })
})

describe('DELETE BICICLETAS /delete', () => {
    it('status 200', (done) => {
        var headers = { 'Content-Type': 'application/json' };

        Axios.post('http://localhost:3000/api/bicicletas/delete', {
            id: 14
        })
            .then(function (response) {
                expect(response.status).toBe(204);
            }).catch(function (error) {
                console.log(error);
            })

        Axios.get('http://localhost:3000/api/bicicletas')
            .then(function (response) {
                expect(response.status).toBe(200);
                expect(response.data.bicicletas.find(x => x.id = 14)).toBe(undefined);
                done();
            }).catch(function (error) {
                console.log(error);
            })
    })
})

describe('UPDATE BICICLETAS /:id/update', () => {
    it('status 200', (done) => {
        Axios.post('http://localhost:3000/api/bicicletas/create', {
            id: 5, color: 'blanco', modelo: 'BMX', lat: -12, lng: 65
        })
            .then(function (response) {
                expect(response.status).toBe(200);
            }).catch(function (error) {
                console.log(error);
            })


        Axios.post('http://localhost:3000/api/bicicletas/5/update', {
            id: 7, color: 'Purpura', modelo: 'Todoterreno', lat: -4, lng: 12
        })
        .then(function (response){
            expect(response.status).toBe(200);
        }).catch(function(error){
            console.log(error);
        })

        Axios.get('http://localhost:3000/api/bicicletas')
        .then(function(reponse){
            expect(reponse.status).toBe(200);
            expect(reponse.data.bicicletas.find(bici => bici.id = 7).color).toBe('Purpura');
            done();
        })
    })
})